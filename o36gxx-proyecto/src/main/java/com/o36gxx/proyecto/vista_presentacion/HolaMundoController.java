package com.o36gxx.proyecto.vista_presentacion;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Agragamos las siguientes anotaciones
@RestController
@RequestMapping("/saludar")  // responde bajo la ruta saludar
public class HolaMundoController {

    // Creamos un nuevo controlador
    @GetMapping("/hola") // el método responde bajo la anotación GetMapping
    public String saludar(){

        return "<br> <h1>Hola Tripulantes</h1>... <h3>Primer ejemplo con Spring Boot...</h3>";

    }
}
