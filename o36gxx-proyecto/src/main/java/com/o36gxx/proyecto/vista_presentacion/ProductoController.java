package com.o36gxx.proyecto.vista_presentacion;

import com.o36gxx.proyecto.controlador_persistencia.Producto;
import com.o36gxx.proyecto.modelo_logica.repository_service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ProductoController {

    // Atributos
    @Autowired  // logra conectar y utilizar el repositorio desde la clase.
    private ProductoService productoService;

    // Métodos o Funciones

    //Create
    @PutMapping(value = "/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto){
        Producto obj = productoService.save(producto);
        return new ResponseEntity<Producto>(obj, HttpStatus.OK);
    }

    // Read
    @GetMapping("/list")
    public List<Producto> consultarTodo(){
        return productoService.findAll();
    }

    @GetMapping("/list/{id}")
    public Producto consultaPorId(@PathVariable("id") Integer idProd){
        return productoService.findById(idProd);
    }

    // Update
    @PostMapping(value = "/")
    public ResponseEntity<Producto> editar(@RequestBody Producto producto){
        Producto obj = productoService.findById(producto.getIdProd());
        if (obj != null){
            obj.setNombreProd(producto.getNombreProd());
            obj.setPreciocompra_prod(producto.getPreciocompra_prod());
            obj.setPrecioventa_prod(producto.getPrecioventa_prod());
            obj.setCantidad_prod(producto.getCantidad_prod());
            productoService.save(obj);
            return new ResponseEntity<Producto>(obj, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Producto>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable("id") Integer idProd){
        Producto obj = productoService.findById(idProd);
        if (obj != null){
            productoService.delete(idProd);
            return new ResponseEntity<Producto>(obj, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Producto>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
