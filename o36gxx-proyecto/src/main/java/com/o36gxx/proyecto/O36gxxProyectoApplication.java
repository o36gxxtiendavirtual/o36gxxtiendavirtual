package com.o36gxx.proyecto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O36gxxProyectoApplication {

	public static void main(String[] args) {
		SpringApplication.run(O36gxxProyectoApplication.class, args);
	}

}
