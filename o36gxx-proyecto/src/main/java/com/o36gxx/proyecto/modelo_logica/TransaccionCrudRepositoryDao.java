package com.o36gxx.proyecto.modelo_logica;

import com.o36gxx.proyecto.controlador_persistencia.Transaccion;
import org.springframework.data.repository.CrudRepository;

public interface TransaccionCrudRepositoryDao extends CrudRepository<Transaccion, Integer> {
}
