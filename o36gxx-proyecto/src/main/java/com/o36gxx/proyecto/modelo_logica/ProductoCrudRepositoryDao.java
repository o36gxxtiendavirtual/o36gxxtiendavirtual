package com.o36gxx.proyecto.modelo_logica;

import com.o36gxx.proyecto.controlador_persistencia.Producto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductoCrudRepositoryDao extends CrudRepository<Producto, Integer> {

    // Forma tradicional
    // @Query(value = "SELECT * FROM producto", nativeQuery = true)

    // Nueva Forma
    // List<Producto> findAll();
}
