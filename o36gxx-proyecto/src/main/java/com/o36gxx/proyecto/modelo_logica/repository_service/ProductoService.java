package com.o36gxx.proyecto.modelo_logica.repository_service;

import com.o36gxx.proyecto.controlador_persistencia.Producto;

import java.util.List;

public interface ProductoService {

    public Producto save(Producto producto);
    public void delete(Integer idProd);
    public Producto findById(Integer idProd);
    public List<Producto> findAll();
}
